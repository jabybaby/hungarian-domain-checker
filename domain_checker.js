const axios = require("axios")

async function isFree(domainName, ending = "hu") {
    return new Promise(async (resolve, reject) => {
        if (domainName == undefined || !domainName) {
            reject("You have to provide a domain name.");
        }
        else {
            data = await check(domainName, ending);

            resolve(!(data.includes("nem szabad") || data.includes("Érvénytelen") || data.includes("közdomain alatt védett név") || data.includes("Védett név")));
        }
    })
}

async function check(domainName, ending = "hu") {
    return new Promise(async (resolve, reject) => {
        if (domainName == undefined || !domainName) {
            reject("You have to provide a domain name.");
        }
        else {
            let nonce = await getNonce()
            let post_data = `action=landingform&ajax_nonce=${nonce}&domainAddress=${domainName}&domainEnding=hu&valid=true`;
            let data = await axios.post("https://www.domain.hu/wp/wp-admin/admin-ajax.php", post_data, {
                headers: {
                    "host": "www.domain.hu",
                    "user-agent": "Mozilla/ 5.0(X11; Linux x86_64; rv: 100.0) Gecko / 20100101 Firefox / 100.0",
                    "accept": "*/*",
                    "accept-language": "en-US,en;q=0.5",
                    "accept-encoding": "gzip, deflate, br",
                    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                    "x-requested-with": "XMLHttpRequest",
                    "content-length": post_data.length,
                    "origin": "https://www.domain.hu",
                    "connection": "keep-alive",
                    "referer": "https://www.domain.hu/",
                    "sec-fetch-dest": "empty",
                    "sec-fetch-mode": "cors",
                    "sec-fetch-site": "same-origin",
                    "pragma": "no-cache",
                    "cache-control": "no-cache",
                }
            }).catch(e => console.log(e))
            data = data.data
            resolve(HTMLPartToTextPart(data));
        }
    })
}

async function getNonce() {
    return new Promise(async (resolve, reject) => {
        let data = await axios.get("https://www.domain.hu/")
        data = data.data.split("nonce\":\"");
        data.shift();
        data.shift();
        data = data[0].split('"')[0];
        return resolve(data);
    })
}

const HTMLPartToTextPart = (HTMLPart) => (
    HTMLPart
        .replace(/\n/ig, '')
        .replace(/<style[^>]*>[\s\S]*?<\/style[^>]*>/ig, '')
        .replace(/<head[^>]*>[\s\S]*?<\/head[^>]*>/ig, '')
        .replace(/<script[^>]*>[\s\S]*?<\/script[^>]*>/ig, '')
        .replace(/<\/\s*(?:p|div)>/ig, '\n')
        .replace(/<br[^>]*\/?>/ig, '\n')
        .replace(/<[^>]*>/ig, '')
        .replace('&nbsp;', ' ')
        .replace(/[^\S\r\n][^\S\r\n]+/ig, ' ')
);

const makeid = (length) => {
    var result = '';
    var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

module.exports = {isFree, check}
